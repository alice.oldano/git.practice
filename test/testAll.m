% include the root folder and all subfolders.
addpath(genpath(pwd));

% define a success exit code
exit_code = 0;

try

    % save the userpath
    originalUserPath = path;

    result = runtests('./test/', 'Recursively', true, 'BaseFolder', '*suite*');

    sumFailed = 0;
    sumIncomplete = 0;

    % print out a summary table
    table(result)

    for i = 1:size(result,2)
        sumFailed = sumFailed + result(i).Failed
        sumIncomplete = sumIncomplete + result(i).Incomplete
    end

    % restore the original path
    restoredefaultpath;
    addpath(originalUserPath);

    if sumFailed > 0 || sumIncomplete > 0
        exit_code = 1;
    end

    % ensure that we ALWAYS call exit
    if ~isempty(strfind(getenv('HOME'), 'jenkins'))
        exit(exit_code);
    end
catch
    exit(1);
end